---
slug: welcome
title: Welcome
authors: [ino]
tags: [LittlePocket.Lab, Greeting]
---

## ようこそ。
このウェブサイトは、LittlePocket.Labがウェブサイトの依頼を効率よく受託する際に利用するものとしてリリースしました。

ウェブサイトをLittlePocket.Labに依頼する際、お客様側にも多くを知っていただきたい。

お互いが制作物に対する知識を有していることが、うまく進行する上で重要です。