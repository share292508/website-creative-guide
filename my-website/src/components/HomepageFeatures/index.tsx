import clsx from 'clsx';
import Heading from '@theme/Heading';
import styles from './styles.module.css';

type FeatureItem = {
  title: string;
  Svg: React.ComponentType<React.ComponentProps<'svg'>>;
  description: JSX.Element;
  optional?: string;
};

const FeatureList: FeatureItem[] = [
  {
    title: 'ウェブの基礎知識',
    Svg: require('@site/static/img/undraw_docusaurus_mountain.svg').default,
    description: (
      <>
        ウェブサイトやその周辺知識に関するドキュメントを作成しています。
        教養として、ウェブの知識を習得しよう。
      </>
    ),
  },
  {
    title: '依頼前の下準備',
    Svg: require('@site/static/img/undraw_docusaurus_tree.svg').default,
    description: (
      <>
        ウェブサイトは作り手に左右されます。
        作り手に正確な情報を伝えるための方法をお伝えしましょう。
      </>
    ),
  },
  {
    title: '新たな発見',
    Svg: require('@site/static/img/undraw_docusaurus_react.svg').default,
    description: (
      <>
        このドキュメントは、ウェブに関する様々なアイディアを集めました。
        ここに来る前とは違った視点でウェブを観察できるようになっていれば、最高です。
      </>
    ),
  },
];

function Feature({title, Svg, description, optional}: FeatureItem) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} role="img" />
      </div>
      <div className="text--center padding-horiz--md">
        <Heading as="h3">{title}</Heading>
        <p>{description}</p>
        <p>{optional}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures(): JSX.Element {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
