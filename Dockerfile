FROM node:22.3.0
# 変数の定義
ARG WORK_DIR=project/my-website
# 必要最低限のツールを入れる
RUN apt-get update -qq && apt-get install -y vim nodejs npm
Add . .
WORKDIR $WORK_DIR
# RUN npm install
EXPOSE 3000