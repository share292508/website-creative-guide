## Published URL
- https://website-creative-guide-share292508-309c97e59b346761ed84f11bbf4c.gitlab.io/

## Overview
> [Docusaurus](https://docusaurus.io/)
- LittlePocket.Labにウェブサイトを依頼する際の、事前資料。

## Get Started Development!!
```bash
docker compose up -d --build

npm run serve

docker compose exec node npm run build
```


## Deploy
- mainブランチに変更があると、CIでGitLabPageに自動デプロイされる。
- 特にサーバー等は用意していない。
- GitLabの無料ホスティングサービスを使用。